import base64
import json
import requests
import aiohttp


class SpotifyApiProvider:
    access_token = None

    def __init__(self, client_id: str, client_secret: str):
        self.client_id = client_id
        self.client_secret = client_secret

    def authenticate(self):
        token_data = {'grant_type': 'client_credentials'}
        headers = {'Authorization': f'Basic {self._client_creds_64(self.client_id, self.client_secret)}'}

        token_response = requests.post('https://accounts.spotify.com/api/token', headers=headers, data=token_data)
        token_data = token_response.json()

        self.access_token = token_data['access_token']

    def _client_creds_64(self, client_id, client_secret):
        client_creds = f'{client_id}:{client_secret}'
        client_creds_b64 = base64.b64encode(client_creds.encode())
        return client_creds_b64.decode()

    async def get(self, url: str) -> json:
        # TODO: add a check for expired token and re-authenticate if necessary
        if self.access_token is None:
            self.authenticate()

        headers = {'Authorization': f'Bearer {self.access_token}'}
        async with aiohttp.ClientSession() as session:
            async with session.get(url, headers=headers) as response:
                return await response.json()

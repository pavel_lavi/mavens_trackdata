import asyncio
import time
from typing import List
from featured_songs import FeaturedSong
from track_data import TrackData


def track_data(category: str, country_code: str) -> List[FeaturedSong]:
    # TODO: add your implementation here. implement & call other methods as necessary   
    client_id = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    client_secret = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

    track_data_instance = TrackData(client_id, client_secret)
    return asyncio.run(track_data_instance.get_songs_metadata(category, country_code))


if __name__ == '__main__':
    timer = time.time()
    result_logs = track_data('pop', 'US')
    time_took = time.time() - timer
    assert result_logs and isinstance(result_logs, list)
    assert all([isinstance(log, FeaturedSong) for log in result_logs])
    print(f'Track complete, produced {len(result_logs)} logs and took {time_took:.5} seconds')
import pytest
from track_data import TrackData
from unittest.mock import patch, AsyncMock


@pytest.fixture(scope='function')
def track_data():
    return TrackData('client_id', 'client_secret')


@pytest.mark.parametrize('track_test_data', [({
    'track': {
        'id': '1',
        'name': '2',
        'album': {
            'name': 'bla',
            'total_tracks': 4
        },
        'artists': [
            {
                'name': 'blu'
            }
        ]
    }
}),
    ({
        'track': {
            'id': '1',
            'name': '2',
            'album': {
                'total_tracks': 4
            },
            'artists': [
                {
                    'name': 'blu'
                }
            ]
        }
    }),
    ({
        'track': {
            'id': '1',
            'name': '2',
            'album': {
                'name': 'bla',
            },
            'artists': [
                {
                    'name': 'blu'
                }
            ]
        }
    }),
    ({
        'track': {
            'id': '1',
            'name': '2',
            'album': {
                'name': 'bla',
            },
            'artists': []
        }
    })])
def test_track_data_process_track_metadata_featuredsong_obj(track_test_data, track_data):
    featured_song = track_data._construct_song_metadata_object('6', '7', 8, 9, track_test_data)

    assert featured_song.playlist_id == '6'
    assert featured_song.playlist_name == '7'
    assert featured_song.playlist_rank == 8
    assert featured_song.track_id == track_test_data['track']['id']
    assert featured_song.track_name == track_test_data['track']['name']
    assert featured_song.track_rank == 9

    if len(track_test_data['track']['artists']) > 0:
        assert featured_song.artist_names == [track_test_data['track']['artists'][0]['name']]
    if 'name' in track_test_data['track']['album']:
        assert featured_song.album_name == track_test_data['track']['album']['name']
    if 'total_tracks' in track_test_data['track']['album']:
        assert featured_song.album_tracks == track_test_data['track']['album']['total_tracks']


@pytest.mark.asyncio
async def test_track_data_process_playlist_metadata_request_data(track_data):
    with patch('track_data.TrackData._api_provider', new=AsyncMock) as mock_api_provider:
        mock_api_provider.get = AsyncMock(return_value={'tracks': {
            'items': [
                {
                    'track': {
                        'id': '1',
                        'name': '2',
                        'album': {
                            'name': 'bla',
                            'total_tracks': 4
                        },
                        'artists': [
                            {
                                'name': 'blu'
                            }
                        ]
                    }
                },
                {
                    'track': {
                        'id': '5',
                        'name': '6',
                        'album': {
                            'name': 'bla',
                            'total_tracks': 4
                        },
                        'artists': [
                            {
                                'name': 'blu'
                            }
                        ]
                    }
                }
            ]
        }})
        result = await track_data._construct_song_metadata_object(1, '1', 1)

        mock_api_provider.call_count = 1

    assert len(result) == 2
    assert result[0].track_id == '1' or result[1].track_id == '1'
    assert result[1].track_id == '5' or result[0].track_id == '5'

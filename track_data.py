import asyncio
from typing import List
from featured_songs import FeaturedSong
from spotify_api_provider import SpotifyApiProvider


class TrackData:
    _api_provider = None

    def __init__(self, client_id: str, client_secret: str):
        self.client_id = client_id
        self.client_secret = client_secret

    def initialize(self):
        api_provider = SpotifyApiProvider(self.client_id, self.client_secret)
        api_provider.authenticate()
        self._api_provider = api_provider

    async def get_songs_metadata(self, category: str, country_code: str) -> List[FeaturedSong]:
        if self._api_provider is None:
            self.initialize()

        results = []
        categories_data = await self._api_provider.get(
            f'https://api.spotify.com/v1/browse/categories/{category}/playlists?country={country_code}')
        playlists = categories_data['playlists']['items']
        tasks = [
            *(self._process_playlist_metadata(playlist['id'], playlist['name'], playlists.index(playlist))
              for playlist in playlists)]
        playlist_results = await asyncio.gather(*tasks, return_exceptions=True)
        [results.extend(playlist_result) for playlist_result in playlist_results]
        return results

    async def _process_playlist_metadata(
            self, playlist_id: str, playlist_name: str, playlist_index: int) -> List[FeaturedSong]:
        playlist_results = []
        playlist_tracks = await self._api_provider.get(f'https://api.spotify.com/v1/playlists/{playlist_id}')
        tracks = playlist_tracks['tracks']['items']

        for track in tracks:
            song_metadata = self._construct_song_metadata_object(
                playlist_id,
                playlist_name,
                playlist_index,
                tracks.index(track),
                track
            )
            playlist_results.append(song_metadata)

        return playlist_results

    def _construct_song_metadata_object(self,
            playlist_id: str, playlist_name: str, playlist_index: int, track_index: int, track: dict) -> FeaturedSong:
        track = track['track']
        return FeaturedSong(
            track_id=track['id'],
            track_name=track['name'],
            playlist_id=playlist_id,
            playlist_name=playlist_name,
            playlist_rank=playlist_index,
            album_name=track['album']['name'] if 'name' in track['album'] else None,
            album_tracks=track['album']['total_tracks'] if 'total_tracks' in track['album'] else None,
            artist_names=[artist['name'] for artist in track['artists']],
            track_rank=track_index
        )
